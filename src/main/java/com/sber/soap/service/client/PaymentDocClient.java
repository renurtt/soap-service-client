package com.sber.soap.service.client;

import javax.xml.soap.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;

public class PaymentDocClient {
    private final Scanner scanner = new Scanner(System.in);
    private static final String MY_NAMESPACE = "gs";
    private static final String MY_NAMESPACE_URI = "http://sber.com/soap/service";

    /**
     * Interacts with user and maps user's queries
     *
     * @param soapEndpointUrl URL for connection
     */
    public void soapRequest(String soapEndpointUrl) {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Choose request: get, create, delete, get all, exit.");
            switch (scanner.nextLine().toUpperCase()) {
                case "GET":
                    getRequest(soapEndpointUrl);
                    break;
                case "CREATE":
                    createRequest(soapEndpointUrl);
                    break;
                case "DELETE":
                    deleteRequest(soapEndpointUrl);
                    break;
                case "GET ALL":
                    getAllRequest(soapEndpointUrl);
                    break;
                case "UPDATE":
                    updateRequest(soapEndpointUrl);
                    break;
                case "EXIT":
                    return;
                default:
                    System.out.println("Operation not defined!");
            }
            System.out.println();
        }
    }

    /**
     * Creates connection wit soap service
     *
     * @return connection
     */
    private SOAPConnection configureConnection() throws SOAPException {
        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        return soapConnectionFactory.createConnection();
    }

    /**
     * Creates message for soap service
     *
     * @return message
     */
    private SOAPMessage configureMessage() throws SOAPException {
        MessageFactory messageFactory = MessageFactory.newInstance();
        return messageFactory.createMessage();
    }

    /**
     * Provides message body for soap message
     *
     * @param soapMessage message for body
     * @return configured body
     */
    private SOAPBody getBody(SOAPMessage soapMessage) throws SOAPException {
        SOAPPart soapPart = soapMessage.getSOAPPart();

        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration(MY_NAMESPACE, MY_NAMESPACE_URI);

        return envelope.getBody();
    }

    /**
     * Makes request for updating payment doc
     *
     * @param soapEndpointUrl soap service url
     */

    private void updateRequest(String soapEndpointUrl) {
        SOAPBody soapBody;
        SOAPElement soapBodyElem;
        SOAPMessage soapResponse = null;

        try {
            SOAPConnection soapConnection = configureConnection();
            SOAPMessage soapMessage = configureMessage();
            soapBody = getBody(soapMessage);
            soapBodyElem = soapBody.addChildElement("updatePaymentDocRequest", MY_NAMESPACE);
            addPaymentDocToElement(soapBodyElem);
            soapMessage.saveChanges();

            soapResponse = soapConnection.call(soapMessage, soapEndpointUrl);

            soapConnection.close();
        } catch (SOAPException e) {
            System.err.println("\nError occurred while sending SOAP Request to Server!\n");
        } catch (Exception e) {
            System.err.println("\nError occurred while requesting.\n");
        }

        if (soapResponse == null) {
            return;
        }
        try {
            PaymentDoc paymentDoc = PaymentDoc.parse(soapResponse.getSOAPBody().getFirstChild().getFirstChild());
            System.out.println("Successfully updated payment document:");
            System.out.println(paymentDoc);
        } catch (Exception e) {
            System.err.println("\nError occurred while retrieving the result. Does such id exists? The document needs to be created first.\n");
        }
    }

    /**
     * Makes request for getting all payment docs
     *
     * @param soapEndpointUrl soap service url
     */
    private void getAllRequest(String soapEndpointUrl) {
        SOAPBody soapBody;
        SOAPMessage soapResponse = null;

        try {
            SOAPConnection soapConnection = configureConnection();
            SOAPMessage soapMessage = configureMessage();
            soapBody = getBody(soapMessage);
            soapBody.addChildElement("getAllPaymentDocRequest", MY_NAMESPACE);
            soapMessage.saveChanges();

            soapResponse = soapConnection.call(soapMessage, soapEndpointUrl);

            soapConnection.close();
        } catch (SOAPException e) {
            System.err.println("\nError occurred while sending SOAP Request to Server!\n");
        } catch (Exception e) {
            System.err.println("\nError occurred while requesting.\n");
        }

        if (soapResponse == null) {
            return;
        }
        try {
            List<PaymentDoc> paymentDocs = PaymentDoc.parseMultiplePaymentDocs(soapResponse.getSOAPBody().getFirstChild().getFirstChild().getFirstChild().getNodeValue());
            System.out.println("Received " + paymentDocs.size() + " payment documents.");
            for (PaymentDoc paymentDoc :
                    paymentDocs) {
                System.out.println(paymentDoc);
            }
        } catch (Exception e) {
            System.err.println("\nError occurred while retrieving the result. Does any document even exist?\n");
        }
    }

    /**
     * Makes request for removing payment doc by id
     *
     * @param soapEndpointUrl soap service url
     */
    private void deleteRequest(String soapEndpointUrl) {
        SOAPBody soapBody;
        SOAPElement soapBodyElem;
        SOAPMessage soapResponse = null;

        try {
            SOAPConnection soapConnection = configureConnection();
            SOAPMessage soapMessage = configureMessage();
            soapBody = getBody(soapMessage);
            soapBodyElem = soapBody.addChildElement("deletePaymentDocRequest", MY_NAMESPACE);
            addIdToElement(soapBodyElem);
            soapMessage.saveChanges();

            soapResponse = soapConnection.call(soapMessage, soapEndpointUrl);

            soapConnection.close();
        } catch (SOAPException e) {
            System.err.println("\nError occurred while sending SOAP Request to Server!\n");
        } catch (Exception e) {
            System.err.println("\nError occurred while requesting.\n");
        }

        if (soapResponse == null) {
            return;
        }
        try {
            System.out.println(soapResponse.getSOAPBody().getFirstChild().getFirstChild().getFirstChild().getNodeValue());
        } catch (Exception e) {
            System.err.println("\nError occurred while retrieving the result. \n");
        }
    }

    /**
     * Makes request for creating payment doc
     *
     * @param soapEndpointUrl soap service url
     */
    private void createRequest(String soapEndpointUrl) {
        SOAPBody soapBody;
        SOAPElement soapBodyElem;
        SOAPMessage soapResponse = null;

        try {
            SOAPConnection soapConnection = configureConnection();
            SOAPMessage soapMessage = configureMessage();
            soapBody = getBody(soapMessage);
            soapBodyElem = soapBody.addChildElement("createPaymentDocRequest", MY_NAMESPACE);
            addPaymentDocToElement(soapBodyElem);
            soapMessage.saveChanges();

            soapResponse = soapConnection.call(soapMessage, soapEndpointUrl);

            soapConnection.close();
        } catch (SOAPException e) {
            System.err.println("\nError occurred while sending SOAP Request to Server!\n");
        } catch (Exception e) {
            System.err.println("\nError occurred while requesting.\n");
        }

        if (soapResponse == null) {
            return;
        }
        try {
            PaymentDoc paymentDoc = PaymentDoc.parse(soapResponse.getSOAPBody().getFirstChild().getFirstChild());
            System.out.println("Successfully Created payment document:");
            System.out.println(paymentDoc);
        } catch (Exception e) {
            System.err.println("\nError occurred while retrieving the result.\n");
        }
    }

    /**
     * Makes request for getting payment doc by id
     *
     * @param soapEndpointUrl soap service url
     */
    private void getRequest(String soapEndpointUrl) {
        SOAPBody soapBody;
        SOAPElement soapBodyElem;
        SOAPMessage soapResponse = null;

        try {
            SOAPConnection soapConnection = configureConnection();
            SOAPMessage soapMessage = configureMessage();
            soapBody = getBody(soapMessage);
            soapBodyElem = soapBody.addChildElement("getPaymentDocRequest", MY_NAMESPACE);
            addIdToElement(soapBodyElem);
            soapMessage.saveChanges();

            soapResponse = soapConnection.call(soapMessage, soapEndpointUrl);

            soapConnection.close();
        } catch (SOAPException e) {
            System.err.println("\nError occurred while sending SOAP Request to Server!\n");
        } catch (Exception e) {
            System.err.println("\nError occurred while requesting.\n");
        }

        if (soapResponse == null) {
            return;
        }
        try {
            PaymentDoc paymentDoc = PaymentDoc.parse(soapResponse.getSOAPBody().getFirstChild().getFirstChild());
            System.out.println(paymentDoc);
        } catch (Exception e) {
            System.err.println("\nError occurred while retrieving the result. Does such document exist?\n");
        }
    }

    /**
     * Interacts with user to read id and writes it to soap element
     *
     * @param soapBodyElem body to be written in
     */
    public void addIdToElement(SOAPElement soapBodyElem) throws SOAPException {
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("id", MY_NAMESPACE);

        System.out.println("Enter ID:");
        while (!scanner.hasNextInt()) {
            System.out.println("Not integer value provided. Try again:");
            scanner.nextLine();
        }
        int id = scanner.nextInt();
        soapBodyElem1.addTextNode(Integer.toString(id));
    }

    /**
     * Interacts with user to read all payment doc parameters and writes it to soap element
     * Caution: does not check input validity. Incorrect input either causes exception, or leads to error-response from server
     *
     * @param soapBodyElem body to be written in
     */
    public void addPaymentDocToElement(SOAPElement soapBodyElem) throws SOAPException {
        SOAPElement soapBodyElemDoc = soapBodyElem.addChildElement("paymentDoc", MY_NAMESPACE);
        SOAPElement soapBodyElemDocId = soapBodyElemDoc.addChildElement("id", MY_NAMESPACE);
        System.out.println("Enter ID:");
        soapBodyElemDocId.addTextNode(Integer.toString(scanner.nextInt()));

        SOAPElement soapBodyElemDocPurpose = soapBodyElemDoc.addChildElement("purpose", MY_NAMESPACE);
        System.out.println("Enter purpose:");
        scanner.nextLine();
        soapBodyElemDocPurpose.addTextNode(scanner.nextLine());

        SOAPElement soapBodyElemDocAmount = soapBodyElemDoc.addChildElement("amount", MY_NAMESPACE);
        System.out.println("Enter amount:");
        soapBodyElemDocAmount.addTextNode(new BigDecimal(scanner.nextLine()).toString());

        SOAPElement soapBodyElemDocWriteOffAcc = soapBodyElemDoc.addChildElement("writeOffAccount", MY_NAMESPACE);
        System.out.println("Enter write-off account:");
        soapBodyElemDocWriteOffAcc.addTextNode(scanner.nextLine());

        SOAPElement soapBodyElemDocRecipientAcc = soapBodyElemDoc.addChildElement("recipientAccount", MY_NAMESPACE);
        System.out.println("Enter recipient account:");
        soapBodyElemDocRecipientAcc.addTextNode(scanner.nextLine());
    }
}

package com.sber.soap.service.client;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class PaymentDoc {
    private Integer id;
    private String purpose;
    private BigDecimal amount;
    private String writeOffAccount;
    private String recipientAccount;

    /**
     * Parses string of concatenated payment docs
     *
     * @param paymentDocsString string of concatenated payment docs
     * @return list of parsed docs
     */
    public static List<PaymentDoc> parseMultiplePaymentDocs(String paymentDocsString) {
        List<PaymentDoc> paymentDocs = new ArrayList<>();
        String[] tokenizedPaymentDocs = paymentDocsString.split(";");
        for (String textPaymentDoc :
                tokenizedPaymentDocs) {
            paymentDocs.add(parsePaymentDoc(textPaymentDoc));
        }
        return paymentDocs;
    }

    /**
     * Parses string of one payment doc
     *
     * @param textPaymentDoc string of one payment doc, properties are separated by comas
     * @return parsed payment doc
     */
    private static PaymentDoc parsePaymentDoc(String textPaymentDoc) {
        String[] paymentDocProperties = textPaymentDoc.split(",");
        if (paymentDocProperties.length != 5) {
            throw new RuntimeException("Parsing failed.");
        }

        PaymentDoc paymentDoc = new PaymentDoc();
        paymentDoc.setId(Integer.valueOf(paymentDocProperties[0]));
        paymentDoc.setPurpose(paymentDocProperties[1]);
        paymentDoc.setAmount(new BigDecimal(paymentDocProperties[2]));
        paymentDoc.setWriteOffAccount(paymentDocProperties[3]);
        paymentDoc.setRecipientAccount(paymentDocProperties[4]);
        return paymentDoc;
    }

    /**
     * Parses payment doc from XML
     *
     * @param paymentDocNode XML-node of payment doc
     * @return parsed payment doc
     */
    public static PaymentDoc parse(Node paymentDocNode) {
        NodeList paymentDocProperties = paymentDocNode.getChildNodes();

        if (paymentDocProperties.getLength() != 5) {
            throw new RuntimeException("Parsing failed.");
        }

        PaymentDoc paymentDoc = new PaymentDoc();
        paymentDoc.setId(Integer.valueOf(paymentDocProperties.item(0).getFirstChild().getNodeValue()));

        paymentDoc.setPurpose(paymentDocProperties.item(1).getFirstChild().getNodeValue());
        paymentDoc.setAmount(new BigDecimal(paymentDocProperties.item(2).getFirstChild().getNodeValue()));
        paymentDoc.setWriteOffAccount(paymentDocProperties.item(3).getFirstChild().getNodeValue());
        paymentDoc.setRecipientAccount(paymentDocProperties.item(4).getFirstChild().getNodeValue());
        return paymentDoc;
    }

    /**
     * `Concatenates parameters of doc to be printed
     *
     * @return string containing properties values
     */
    @Override
    public String toString() {
        return "Payment Document{" +
                "id=" + id +
                ", purpose='" + purpose + '\'' +
                ", amount=" + amount +
                ", write-off account='" + writeOffAccount + '\'' +
                ", recipient account='" + recipientAccount + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getWriteOffAccount() {
        return writeOffAccount;
    }

    public void setWriteOffAccount(String writeOffAccount) {
        this.writeOffAccount = writeOffAccount;
    }

    public String getRecipientAccount() {
        return recipientAccount;
    }

    public void setRecipientAccount(String recipientAccount) {
        this.recipientAccount = recipientAccount;
    }
}

package com.sber.soap.service.client;

public class PaymentDocClientApplication {
    public static void main(String[] args) {
        String soapEndpointUrl = "http://localhost:8080/ws";

        PaymentDocClient paymentDocClient = new PaymentDocClient();
        paymentDocClient.soapRequest(soapEndpointUrl);
    }
}
